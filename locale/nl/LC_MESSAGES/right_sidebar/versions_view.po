# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 00:40+0000\n"
"PO-Revision-Date: 2023-01-15 17:22+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: ../../right_sidebar/versions_view.rst:1
msgid "digiKam Right Sidebar Versions View"
msgstr "De versieweergave in de rechter zijbalk van digiKam"

#: ../../right_sidebar/versions_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, versioning"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, versiebeheer"

#: ../../right_sidebar/versions_view.rst:14
msgid "Versions View"
msgstr "Versieweergave"

#: ../../right_sidebar/versions_view.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../right_sidebar/versions_view.rst:18
msgid ""
"The **Versions** tab shows the history and the saved versions of a "
"photograph. With the three buttons in the top right corner you can choose "
"between a simple list of the saved versions, a tree view and a combined list "
"that shows the versions together with the actions performed with the "
"selected photograph."
msgstr ""
"Het tabblad **Versies** toont de geschiedenis en de opgeslagen versies van "
"een foto. Met de drie knoppen in rechterbovenhoek kunt u kiezen tussen een "
"eenvoudige lijst van de opgeslagen versies, een boomstructuurweergave en een "
"gecombineerde lijst die de versies toont samen met de acties uitgevoerd met "
"de geselecteerde foto."

#: ../../right_sidebar/versions_view.rst:24
msgid "The Versions View From Right Sidebar Displaying Tree-View"
msgstr ""
"De Versieweergave in de rechte zijbalk die een boomstructuurweergave toont"

#: ../../right_sidebar/versions_view.rst:26
msgid ""
"The tree view shows the parent and child versions of the selected image. "
"Here the second and the fifth version derive directly from the original "
"image, the third and forth version are children of the second version."
msgstr ""
"De boomstructuurweergave toont de ouder- en afgeleide versies van de "
"geselecteerde afbeelding. Hier zijn de tweede en de vijfde versie direct "
"afgeleid van de originele afbeelding, de derde en vierde versie zijn "
"dochters van de tweede versie."

#: ../../right_sidebar/versions_view.rst:32
msgid "The Versions View From Right Sidebar Displaying Flat-List"
msgstr "De Versieweergave in de rechte zijbalk die een vlakke lijst toont"

#: ../../right_sidebar/versions_view.rst:34
msgid ""
"The combined list shows the versions together with the actions/filters "
"applied to them. Here the second version was created by resizing the "
"original, applying the unsharp mask, correcting the white balance and "
"finally adding a frame with the border tool. The child versions are grouped "
"in Derived Versions and Related Versions. Related Versions arise if you "
"modify the original and save the changes with **Save As New Version**."
msgstr ""
"De gecombineerde lijst toont de versies samen met de er op toegepaste acties/"
"filters. Hier was de tweede versie gemaakt door het origineel in grootte te "
"wijzigen, met toepassen van het masker voor scherpte verminderen, correctie "
"van de witbalance en tenslotte een frame toevoegen met het hulpmiddel rand. "
"De afgeleide versies zijn gegroepeerd in Afgeleide versies en Gerelateerde "
"versies. Gerelateerde versies verschijnen als u het origineel wijzigt en de "
"wijzigingen opslaat met **Opslaan als nieuwe versie**."

#: ../../right_sidebar/versions_view.rst:36
msgid ""
"To learn how to control what is stored as a separate version and which "
"versions are displayed in the Image Area see :ref:`Image Versioning Settings "
"<editor_settings>`."
msgstr ""
"Om te leren hoe te besturen wat wordt opgeslagen als een aparte versie en "
"welke versies getoond worden in het Afbeeldingsgebied zie :ref:`Instellingen "
"voor versies van afbeeldingen <editor_settings>`."
