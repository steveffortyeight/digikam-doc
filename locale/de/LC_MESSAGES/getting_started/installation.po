# German translations for Digikam Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-18 00:51+0000\n"
"PO-Revision-Date: 2023-01-01 12:31+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../getting_started/installation.rst:1
msgid "How to Install digiKam Photo Management Program"
msgstr ""

#: ../../getting_started/installation.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, install, linux, windows, macos, requirements, configurations, "
"update"
msgstr ""

#: ../../getting_started/installation.rst:14
msgid "Installation"
msgstr ""

#: ../../getting_started/installation.rst:16
msgid "Contents"
msgstr ""

#: ../../getting_started/installation.rst:18
msgid ""
"Visit `the download page <https://www.digikam.org/download/>`_ of the "
"digiKam Web site for up to date information on installing stable **digiKam**."
msgstr ""

#: ../../getting_started/installation.rst:20
msgid ""
"You’ll find all previous digiKam versions in the `attic <https://download."
"kde.org/Attic/digikam/>`_."
msgstr ""

#: ../../getting_started/installation.rst:24
msgid ""
"Bundles using the **-debug** in file-name includes `debug symbols <https://"
"en.wikipedia.org/wiki/Debug_symbol>`_ to catch debugger traces when "
"application stop to work. Files are more heavy but this will help developers "
"to found the origin of dysfunctions. See the :ref:`Get Involved "
"<get_involved>` section for details."
msgstr ""

#: ../../getting_started/installation.rst:27
msgid "Minimum System Requirements"
msgstr ""

#: ../../getting_started/installation.rst:29
msgid ""
"**Operating System**: 64-bit Windows 7 or newer, Apple macOS 64-bit based on "
"Intel or M1 architecture, 64-bit Linux. Details see below."
msgstr ""

#: ../../getting_started/installation.rst:31
msgid ""
"**CPU**: x86 Intel or AMD; at least one 2 GHz core for standard photo "
"processing, 4 cores for large photo or panorama processing, 8 cores for deep-"
"learning uses as with face detection and image quality sorting. Details see "
"below."
msgstr ""

#: ../../getting_started/installation.rst:33
msgid ""
"**GPU**: OpenGL 2.0 that works correctly and is compatible. On Windows, you "
"can also use a card with good, compatible DirectX 9 or 11 drivers."
msgstr ""

#: ../../getting_started/installation.rst:35
msgid ""
"**RAM**: At least 4 GB for standard photo processing, 8 GB for large photo "
"or panorama processing, 16 GB for deep-learning uses as with face detection "
"and image quality sorting. Details see below."
msgstr ""

#: ../../getting_started/installation.rst:39
msgid ""
"If your computer is at the lower end of CPU and RAM requirements, you should "
"use both :ref:`Small Thumbnails Resolution <thumbnails_resolution>` and :ref:"
"`Loading Embedded Preview <preview_settings>` features to help reduce "
"preview lag."
msgstr ""

#: ../../getting_started/installation.rst:41
msgid ""
"**Monitors**: If the application can be used without problem on a single "
"screen, using multi-screens will improve the experience especially with the "
"workflow to review items."
msgstr ""

#: ../../getting_started/installation.rst:47
msgid ""
"A Double-Screens Linux Station with **Main Window** on the Left and **Light "
"Table** on the Right"
msgstr ""

#: ../../getting_started/installation.rst:49
msgid ""
"**Storage**: SSDs will be the best choice for robustness and speed to host "
"database and collection."
msgstr ""

#: ../../getting_started/installation.rst:54
msgid "digiKam on Linux"
msgstr ""

#: ../../getting_started/installation.rst:56
msgid "**digiKam** can be installed on non-KDE Desktops without any issues."
msgstr ""

#: ../../getting_started/installation.rst:58
msgid "**AppImage:** Minimum Ubuntu 18.04 has no such minimal requirements."
msgstr ""

#: ../../getting_started/installation.rst:60
msgid ""
"`AppImage <https://en.wikipedia.org/wiki/AppImage>`_ is a bundle hosting all "
"the necessary dependencies to run digiKam. Nothing is installed on your "
"system. Turn on **executable** property of AppImage file and run it. That "
"all..."
msgstr ""

#: ../../getting_started/installation.rst:68
msgid "Screencast of AppImage Startup"
msgstr ""

#: ../../getting_started/installation.rst:74
msgid ""
"The AppImage bundle includes also the :ref:`Showfoto stand Alone Image "
"Editor <showfoto_editor>`. To run it instead digiKam, just add **showfoto** "
"as argument to the AppImage command line, like this:"
msgstr ""

#: ../../getting_started/installation.rst:82
msgid ""
"The AppImage provides more options to start which can be listed with "
"**help** argument from command line."
msgstr ""

#: ../../getting_started/installation.rst:85
msgid "digiKam on Windows"
msgstr ""

#: ../../getting_started/installation.rst:88
msgid "Using on Standard Desktop"
msgstr ""

#: ../../getting_started/installation.rst:90
msgid ""
"**digiKam** runs only on 64bit version of Windows. digiKam runs on Windows 7 "
"and newer. We cannot guarantee that digiKam runs on server or embedded "
"Windows version."
msgstr ""

#: ../../getting_started/installation.rst:92
msgid ""
"digiKam is available as an **installable** (Setup Program) and as a "
"**standalone** (Archive) version."
msgstr ""

#: ../../getting_started/installation.rst:94
msgid ""
"**Installable** version: **Needs** administrator rights and gets installed "
"on your local machine. It's also listed as a program."
msgstr ""

#: ../../getting_started/installation.rst:96
msgid "It's available for all users on your computer."
msgstr ""

#: ../../getting_started/installation.rst:98
msgid "The digiKam files are always located in the same folder."
msgstr ""

#: ../../getting_started/installation.rst:102
msgid ""
"If a previous digiKam version is present on your system, the Windows "
"installer will warns you to uninstall it before to process to the new "
"installation."
msgstr ""

#: ../../getting_started/installation.rst:109
msgid ""
"The Windows Warning When you Try to Install digiKam When a Previous Version "
"is Present"
msgstr ""

#: ../../getting_started/installation.rst:116
msgid "The Windows Dialog to Uninstall Previous Version of digiKam"
msgstr ""

#: ../../getting_started/installation.rst:123
msgid "The Windows Dialog to Install Newer Version of digiKam"
msgstr ""

#: ../../getting_started/installation.rst:125
msgid ""
"**Standalone** version: **Doesn't** need administrator rights and isn't "
"installed. It's **not** listed as a program. Is only accessible for the user "
"who has downloaded the file."
msgstr ""

#: ../../getting_started/installation.rst:127
msgid "If you work with a normal user on your computer, you can use digiKam."
msgstr ""

#: ../../getting_started/installation.rst:129
msgid ""
"You can copy the digiKam folder on any external drive and run it on a "
"different computer without installing it. However, your personal settings "
"and downloads within digiKam are related to the computer you work on."
msgstr ""

#: ../../getting_started/installation.rst:132
msgid "Using in a Windows Domain"
msgstr ""

#: ../../getting_started/installation.rst:134
msgid ""
"If you want to use digiKam with domain users with using Windows Active "
"Directory and/or Group Policies (`GPOs <https://learn.microsoft.com/en-us/"
"previous-versions/windows/desktop/policy/group-policy-objects>`_) make sure "
"all users have read/write rights to the following folders:"
msgstr ""

#: ../../getting_started/installation.rst:138
msgid "%AppData%\\\\digikam"
msgstr ""

#: ../../getting_started/installation.rst:140
msgid "%LocalAppData%\\\\digikam"
msgstr ""

#: ../../getting_started/installation.rst:142
msgid "%LocalAppData%\\\\digikamrc"
msgstr ""

#: ../../getting_started/installation.rst:144
msgid "%LocalAppData%\\\\digikam_systemrc"
msgstr ""

#: ../../getting_started/installation.rst:146
msgid "%LocalAppData%\\\\kxmlgui5\\\\digikam\\digikamui.rc"
msgstr ""

#: ../../getting_started/installation.rst:148
msgid ""
"Similar requirements is also necessary for the :ref:`Showfoto stand Alone "
"Image Editor <showfoto_editor>`:"
msgstr ""

#: ../../getting_started/installation.rst:152
msgid "%AppData%\\\\showfoto"
msgstr ""

#: ../../getting_started/installation.rst:154
msgid "%LocalAppData%\\\\showfoto"
msgstr ""

#: ../../getting_started/installation.rst:156
msgid "%LocalAppData%\\\\showfotorc"
msgstr ""

#: ../../getting_started/installation.rst:158
msgid "%LocalAppData%\\\\showfoto_systemrc"
msgstr ""

#: ../../getting_started/installation.rst:160
msgid "%LocalAppData%\\\\kxmlgui5\\\\digikam\\showfotoui.rc"
msgstr ""

#: ../../getting_started/installation.rst:162
msgid "Do also make sure no GPO is blocking the access to these folders."
msgstr ""

#: ../../getting_started/installation.rst:167
msgid "digiKam on macOS"
msgstr ""

#: ../../getting_started/installation.rst:170
msgid "The non Signed Package"
msgstr ""

#: ../../getting_started/installation.rst:172
msgid ""
"**digiKam** runs with Intel based Mac's on macOS 64-bit. For M1 based Mac's, "
"you needs to use `Apple Rosetta 2 <https://support.apple.com/en-us/"
"HT211861>`_ instructions translator. Minimum system requirement depends of "
"the digiKam version to install:"
msgstr ""

#: ../../getting_started/installation.rst:175
msgid "digiKam version"
msgstr ""

#: ../../getting_started/installation.rst:175
msgid "Supported macOS Release"
msgstr ""

#: ../../getting_started/installation.rst:177
msgid ">= 7.10.0"
msgstr ""

#: ../../getting_started/installation.rst:177
msgid ""
"macOS 10.15 (`Catalina <https://en.wikipedia.org/wiki/MacOS_Catalina>`_) or "
"newer"
msgstr ""

#: ../../getting_started/installation.rst:178
msgid ">= 7.0.0"
msgstr ""

#: ../../getting_started/installation.rst:178
msgid ""
"macOS 10.13 (`High Sierra <https://en.wikipedia.org/wiki/"
"MacOS_High_Sierra>`_) or newer"
msgstr ""

#: ../../getting_started/installation.rst:179
msgid ">= 6.4.0"
msgstr ""

#: ../../getting_started/installation.rst:179
msgid ""
"macOS 10.12 (`Sierra <https://en.wikipedia.org/wiki/MacOS_Sierra>`_) or newer"
msgstr ""

#: ../../getting_started/installation.rst:180
msgid ">= 6.1.0"
msgstr ""

#: ../../getting_started/installation.rst:180
msgid ""
"macOS 10.11 (`El Capitan <https://en.wikipedia.org/wiki/OS_X_El_Capitan>`_) "
"or newer"
msgstr ""

#: ../../getting_started/installation.rst:181
msgid ">= 6.0.0"
msgstr ""

#: ../../getting_started/installation.rst:181
msgid ""
"macOS 10.8 (`Mountain Lion <https://en.wikipedia.org/wiki/"
"OS_X_Mountain_Lion>`_) or newer"
msgstr ""

#: ../../getting_started/installation.rst:186
msgid ""
"Due to use `Macports environment <https://www.macports.org/>`_ to build "
"digiKam PKG, and the rolling release Macports policy applied on time, it's "
"difficult to provide a binary compatibility with older versions of macOS. "
"Also Macports packages are frequently updated for security reasons, and this "
"can require more recent macOS SDK."
msgstr ""

#: ../../getting_started/installation.rst:188
msgid ""
"When the **PKG** file is downloaded, to start the installer, a security "
"warning will appears, because the package is not signed for the Apple "
"Gatekeeper."
msgstr ""

#: ../../getting_started/installation.rst:195
msgid "The macOS Warning When you Try to Run digiKam PKG after Downloading"
msgstr ""

#: ../../getting_started/installation.rst:197
msgid ""
"You needs to give the rights to run from the **macOS Config Panel/Security "
"and Confidentiality** to confirm to Gatekeeper that all is safe here."
msgstr ""

#: ../../getting_started/installation.rst:204
msgid "The macOS Security Panel to Authorize digiKam PKG Installation"
msgstr ""

#: ../../getting_started/installation.rst:206
msgid ""
"When installer is running, follow instructions from the assistant to install "
"application on your computer."
msgstr ""

#: ../../getting_started/installation.rst:213
msgid "The digiKam macOS PKG is Started and Ready to Install"
msgstr ""

#: ../../getting_started/installation.rst:216
msgid "Application Rights"
msgstr ""

#: ../../getting_started/installation.rst:218
msgid ""
"When the installation is done, digiKam will need to access on system "
"resources to run properly. This will be asked by the system when digiKam "
"run, depending of actions performed by user. See below some examples of "
"rights set in the macOS **Security and Privacy** policy configuration panel:"
msgstr ""

#: ../../getting_started/installation.rst:221
msgid ""
"**Automation**: This right is set when you try to open an album in Apple "
"Finder file manager through digiKam."
msgstr ""

#: ../../getting_started/installation.rst:228
msgid "The macOS Security and Privacy Panel with the Automation Rights"
msgstr ""

#: ../../getting_started/installation.rst:230
msgid ""
"**Accessibility**: If you want to change the desktop wallpaper with digiKam, "
"this right must be turned on."
msgstr ""

#: ../../getting_started/installation.rst:237
msgid "The macOS Security and Privacy Panel with the Accessibility Rights"
msgstr ""

#: ../../getting_started/installation.rst:239
msgid ""
"**Files And Folders**: if you place your collections on your computer "
"outside the Photos directory from your personal account, digiKam needs "
"special right to access to contents."
msgstr ""

#: ../../getting_started/installation.rst:246
msgid ""
"The macOS Security and Privacy Panel with the Files and Folders Access Rights"
msgstr ""

#: ../../getting_started/installation.rst:248
msgid ""
"**Full Disk**: This right is mandatory if you use gPhoto2 driver to access "
"on system places to communicate with the device."
msgstr ""

#: ../../getting_started/installation.rst:255
msgid "The macOS Security and Privacy Panel with the Full Disk Access Rights"
msgstr ""

#: ../../getting_started/installation.rst:257
msgid ""
"**Photos**: if you want to share Apple Photos collection from your personal "
"account, you will needs to turn on these rights."
msgstr ""

#: ../../getting_started/installation.rst:264
msgid "The macOS Security and Privacy Panel with the Photos Access Rights"
msgstr ""

#: ../../getting_started/installation.rst:269
msgid "Configuration Files"
msgstr ""

#: ../../getting_started/installation.rst:271
msgid ""
"**digiKam**'s application-wide persistent settings are stored in the "
"following locations, depending on your platform."
msgstr ""

#: ../../getting_started/installation.rst:276
#: ../../getting_started/installation.rst:306
msgid "Linux"
msgstr ""

#: ../../getting_started/installation.rst:277
#: ../../getting_started/installation.rst:307
msgid "Windows"
msgstr ""

#: ../../getting_started/installation.rst:278
#: ../../getting_started/installation.rst:308
msgid "macOS"
msgstr ""

#: ../../getting_started/installation.rst:279
#: ../../getting_started/installation.rst:309
msgid "Description"
msgstr ""

#: ../../getting_started/installation.rst:280
msgid ":file:`~/.config/digikamrc`"
msgstr ""

#: ../../getting_started/installation.rst:281
msgid ":file:`%LocalAppData%\\\\digikamrc`"
msgstr ""

#: ../../getting_started/installation.rst:282
msgid ":file:`~/Library/Preferences/digikamrc`"
msgstr ""

#: ../../getting_started/installation.rst:283
msgid ""
"General settings of the application. Delete this and restart digiKam to "
"reset the application to *factory* settings"
msgstr ""

#: ../../getting_started/installation.rst:284
msgid ":file:`~/.config/digikam_systemrc`"
msgstr ""

#: ../../getting_started/installation.rst:285
msgid ":file:`%LocalAppData%\\\\digikam_systemrc`"
msgstr ""

#: ../../getting_started/installation.rst:286
msgid ":file:`~/Library/Preferences/digikam_systemrc`"
msgstr ""

#: ../../getting_started/installation.rst:287
msgid ""
"System settings of the application. Delete this and restart digiKam to reset "
"the application to *factory* settings"
msgstr ""

#: ../../getting_started/installation.rst:288
msgid ":file:`~/.cache/digikam`"
msgstr ""

#: ../../getting_started/installation.rst:289
msgid ":file:`%LocalAppData%\\\\digikam`"
msgstr ""

#: ../../getting_started/installation.rst:290
msgid ":file:`~/Library/Caches/digikam`"
msgstr ""

#: ../../getting_started/installation.rst:291
#: ../../getting_started/installation.rst:321
msgid "cache location storing temporary files"
msgstr ""

#: ../../getting_started/installation.rst:292
msgid ":file:`~/.local/share/digikam`"
msgstr ""

#: ../../getting_started/installation.rst:293
msgid ":file:`%AppData%\\\\digikam`"
msgstr ""

#: ../../getting_started/installation.rst:294
msgid ":file:`~/Library/Application Support/digikam`"
msgstr ""

#: ../../getting_started/installation.rst:295
#: ../../getting_started/installation.rst:325
msgid "contains downloaded: deep-learning models, internal configuration files"
msgstr ""

#: ../../getting_started/installation.rst:296
msgid ":file:`~/.local/share/kxmlgui5/digikam/digikamui5.rc`"
msgstr ""

#: ../../getting_started/installation.rst:297
msgid ":file:`%LocalAppData%\\\\kxmlgui5\\digikam\\\\digikamui5.rc`"
msgstr ""

#: ../../getting_started/installation.rst:298
msgid ""
":file:`~/Library/Application Support/digikam/kxmlgui5/digikam/digikamui5.rc`"
msgstr ""

#: ../../getting_started/installation.rst:299
#: ../../getting_started/installation.rst:329
msgid "contains UI configuration, if your UI is broken, delete this file"
msgstr ""

#: ../../getting_started/installation.rst:301
msgid ""
"As digiKam, the :ref:`Showfoto stand Alone Image Editor <showfoto_editor>` "
"has also persistent settings stored at similar places:"
msgstr ""

#: ../../getting_started/installation.rst:310
msgid ":file:`~/.config/showfotorc`"
msgstr ""

#: ../../getting_started/installation.rst:311
msgid ":file:`%LocalAppData%\\\\showfotorc`"
msgstr ""

#: ../../getting_started/installation.rst:312
msgid ":file:`~/Library/Preferences/showfotorc`"
msgstr ""

#: ../../getting_started/installation.rst:313
msgid ""
"General settings of the application. Delete this and restart Showfoto to "
"reset the application to *factory* settings"
msgstr ""

#: ../../getting_started/installation.rst:314
msgid ":file:`~/.config/showfoto_systemrc`"
msgstr ""

#: ../../getting_started/installation.rst:315
msgid ":file:`%LocalAppData%\\\\showfoto_systemrc`"
msgstr ""

#: ../../getting_started/installation.rst:316
msgid ":file:`~/Library/Preferences/showfoto_systemrc`"
msgstr ""

#: ../../getting_started/installation.rst:317
msgid ""
"System settings of the application. Delete this and restart Showfoto to "
"reset the application to *factory* settings"
msgstr ""

#: ../../getting_started/installation.rst:318
msgid ":file:`~/.cache/showfoto`"
msgstr ""

#: ../../getting_started/installation.rst:319
msgid ":file:`%LocalAppData%\\\\showfoto`"
msgstr ""

#: ../../getting_started/installation.rst:320
msgid ":file:`~/Library/Caches/showfoto`"
msgstr ""

#: ../../getting_started/installation.rst:322
msgid ":file:`~/.local/share/showfoto`"
msgstr ""

#: ../../getting_started/installation.rst:323
msgid ":file:`%AppData%\\\\showfoto`"
msgstr ""

#: ../../getting_started/installation.rst:324
msgid ":file:`~/Library/Application Support/showfoto`"
msgstr ""

#: ../../getting_started/installation.rst:326
msgid ":file:`~/.local/share/kxmlgui5/showfoto/showfoto ui5.rc`"
msgstr ""

#: ../../getting_started/installation.rst:327
msgid ":file:`%LocalAppData%\\\\kxmlgui5\\showfoto\\\\showfotoui5.rc`"
msgstr ""

#: ../../getting_started/installation.rst:328
msgid ""
":file:`~/Library/Application Support/showfoto/kxmlgui5/showfoto/showfotoui5."
"rc`"
msgstr ""

#: ../../getting_started/installation.rst:333
msgid ""
"The character '**~**' indicates the home directory of the current user from "
"the system."
msgstr ""

#: ../../getting_started/installation.rst:336
msgid "Application Updates"
msgstr ""

#: ../../getting_started/installation.rst:340
msgid ""
"If you process a major version update, as for example from version 7 to "
"version 8, we recommend to backup :ref:`your database files "
"<database_intro>`, as generally internal schema can changes. Typically, "
"digiKam is able to migrate tables from an older version to a new one, and "
"all is done automatically at startup, but in all cases, it's always better "
"to save these important digiKam files before to upgrade the application. "
"Look also the :ref:`Database Backup <database_backup>` section from this "
"manual for more recommendations."
msgstr ""

#: ../../getting_started/installation.rst:342
msgid ""
"If you use a bundle as official Linux AppImage, macOS package, or Windows "
"installer, the application can be updated using the :menuselection:`Help --> "
"Check For New Version menu entry`. Look also the updates settings :ref:`in "
"Behavior page <behavior_settings>` from the configuration dialog."
msgstr ""

#: ../../getting_started/installation.rst:348
msgid "The Dialog to Update Application Using Online Weekly Builds"
msgstr ""
