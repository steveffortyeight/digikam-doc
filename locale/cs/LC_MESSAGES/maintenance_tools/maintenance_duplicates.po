# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Vit Pelcak <vit@pelcak.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-05 00:50+0000\n"
"PO-Revision-Date: 2023-01-31 11:28+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid "digiKam Maintenance Tool to Find Duplicates"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, maintenance, duplicates, similarity"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:14
msgid "Find Duplicates"
msgstr "Najít duplikáty"

#: ../../maintenance_tools/maintenance_duplicates.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../maintenance_tools/maintenance_duplicates.rst:22
msgid "The digiKam Maintenance Options to Find Duplicates"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:24
msgid ""
"The **Find Duplicates** Tool is doing the same as the Find duplicates button "
"in the :ref:`the Similarity View <similarity_view>`, but here you can "
"combine it with other maintenance operations and you have the chance to "
"check **Work on all processor cores** under :ref:`Common Options "
"<maintenance_common>` to speed up the process."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:30
msgid "The digiKam Find Duplicates Button from Similarity Left Sidebar"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:32
msgid "This process provides two options to find duplicates items:"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:34
msgid ""
"**Similarity Range**: the lower and higher values to define the range of "
"similarity in percents."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:36
msgid ""
"**Restriction**: this option restrict the duplicate search with some "
"criteria, as to limit search to the album of reference image, or to exclude "
"the album of reference image of the search."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:38
msgid ""
"While the find duplicates process is under progress, notification on the "
"bottom right of main windows will be visible to indicate the amount of items "
"already done."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:44
msgid "The digiKam Find Duplicates Process Running in the Background"
msgstr ""
