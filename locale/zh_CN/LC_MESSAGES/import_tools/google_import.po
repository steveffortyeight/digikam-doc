msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-25 00:52+0000\n"
"PO-Revision-Date: 2023-03-27 12:00\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/digikam-doc/"
"docs_digikam_org_import_tools___google_import.pot\n"
"X-Crowdin-File-ID: 41817\n"

#: ../../import_tools/google_import.rst:1
msgid "digiKam Import from Google Web-Service"
msgstr ""

#: ../../import_tools/google_import.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, google, impport"
msgstr ""

#: ../../import_tools/google_import.rst:14
msgid "Import from Google"
msgstr ""

#: ../../import_tools/google_import.rst:16
msgid "Contents"
msgstr ""

#: ../../import_tools/google_import.rst:19
msgid "Overview"
msgstr ""

#: ../../import_tools/google_import.rst:21
msgid ""
"This tool allows the user to upload photos to the `Google Photo <https://en."
"wikipedia.org/wiki/Google_Photos>`_ web services."
msgstr ""

#: ../../import_tools/google_import.rst:23
msgid ""
"**Google Photos** is a photo sharing which automatically analyzes photos, "
"identifying various visual features and subjects. Users can search for "
"anything in photos including faces and group similar ones together."
msgstr ""

#: ../../import_tools/google_import.rst:25
msgid ""
"The tool can be used to download files from the remote Google Photo server "
"to the image collections on your computer using the Internet. Use the :"
"menuselection:`Import --> Import from Google Photo` :kbd:`Alt+Shift+P` menu "
"entry to access to this tool, or use the icon from the **Tools** tab in the "
"Right Sidebar."
msgstr ""

#: ../../import_tools/google_import.rst:28
msgid "Login to your Account"
msgstr ""

#: ../../import_tools/google_import.rst:30
msgid ""
"When accessing the tool for the first time you are taken through the process "
"of obtaining a token which is used for authentication purposes. The "
"following dialog will popup and a browser window will be launched to help "
"you loggging in to Google:"
msgstr ""

#: ../../import_tools/google_import.rst:36
msgid "The Google Dialog to Select Account"
msgstr ""

#: ../../import_tools/google_import.rst:38
msgid ""
"After successful signup digiKam will be allowed to send photos to the Google "
"website. You will be presented with the following page on successful signup:"
msgstr ""

#: ../../import_tools/google_import.rst:44
msgid "The Google Dialog to Authorize Application"
msgstr ""

#: ../../import_tools/google_import.rst:46
msgid ""
"Then, simply authorize application and close the web browser. Return to the "
"host application dialog, you will see the interface used to upload photos to "
"Google."
msgstr ""

#: ../../import_tools/google_import.rst:50
msgid ""
"When the tool is invoked for second time, it will remember the previous "
"account automatically."
msgstr ""

#: ../../import_tools/google_import.rst:53
msgid "Download from your Account"
msgstr ""

#: ../../import_tools/google_import.rst:55
msgid ""
"The download dialog for **Google Photo** web-service is presented below:"
msgstr ""

#: ../../import_tools/google_import.rst:61
msgid "The Google Photo Import Dialog"
msgstr ""

#: ../../import_tools/google_import.rst:63
msgid ""
"With the **Album** options, you can select the online folder to get files to "
"download. You can **Reload** the list on the combo-box if online contents "
"have been changed in Google web interface."
msgstr ""

#: ../../import_tools/google_import.rst:65
msgid ""
"Below, the main dialog propose the hierarchy of physical **Albums** from "
"your collection. Selected one target entry to import files, or if you want a "
"new one, just press **New Album** button to create a nested entry in the "
"tree-view."
msgstr ""

#: ../../import_tools/google_import.rst:67
msgid ""
"Press **Start Download** button to transfer items. You can click on the "
"**Close** button to abort the uploading of photos."
msgstr ""
