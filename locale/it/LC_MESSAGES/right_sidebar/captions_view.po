# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Valter Mura <valtermura@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-26 00:50+0000\n"
"PO-Revision-Date: 2023-03-05 21:45+0100\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../right_sidebar/captions_view.rst:1
msgid "digiKam Right Sidebar Captions View"
msgstr "Vista delle didascalie della barra laterale destra di digiKam"

#: ../../right_sidebar/captions_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, description, captions, title, author, labels, rating, date, "
"tags, template"
msgstr ""
"digiKam, documentazione, manuale utente, gestione fotografie, open source, "
"libero, apprendimento, facile, descrizione, didascalie, titolo, autore, "
"etichette, valutazione, data, tag, modello"

#: ../../right_sidebar/captions_view.rst:14
msgid "Captions View"
msgstr "Vista Didascalie"

#: ../../right_sidebar/captions_view.rst:16
msgid "Contents"
msgstr "Contenuto"

#: ../../right_sidebar/captions_view.rst:19
msgid "Overview"
msgstr "Panoramica"

#: ../../right_sidebar/captions_view.rst:21
msgid ""
"This sidebar tab serves to apply and edit image attributes like captions, "
"rating, date and tags. The attributes are stored in the associated database, "
"in the IPTC, XMP, and Exif data fields and become part of the image. All "
"attributes are accessible in one sidebar view as shown in the screenshot "
"below. During image reading the order of priority is ``a)`` database ``b)`` "
"XMP/IPTC and ``c)`` Exif. So if there is a discrepancy between any of the "
"three, this priority will take effect and a synchronization will take place. "
"This sidebar has a first-previous-next-last arrow navigator field on top if "
"shown in the main application."
msgstr ""
"Questa scheda della barra laterale serve ad applicare e a modificare gli "
"attributi dell'immagine, come le didascalie, i voti, le date ed i tag. Gli "
"attributi sono salvati nella banca dati associata, nei campi IPTC, XMP ed "
"EXIF, e diventano parte dell'immagine. Essi sono accessibili in una vista "
"della barra laterale, come mostrato nella schermata sotto. Durante la "
"lettura dell'immagine l'ordine di priorità è: ``a)`` banca dati, ``b)`` XMP/"
"IPTC e ``c)`` Exif. Perciò se ci sono discrepanze tra alcuni di questi sarà "
"considerato questo ordine, e i dati verranno sincronizzati. La barra "
"laterale ha in alto un campo di navigazione con pulsanti del tipo primo-"
"precedente-successivo-ultimo se viene visualizzata nell'applicazione "
"principale."

#: ../../right_sidebar/captions_view.rst:27
msgid "The Captions View From Right Sidebar Displaying Description Information"
msgstr ""
"La vista didascalie dalla barra laterale destra che visualizza le "
"informazioni delle descrizioni"

#: ../../right_sidebar/captions_view.rst:32
msgid "Comment Editors"
msgstr "Editor dei commenti"

#: ../../right_sidebar/captions_view.rst:34
msgid ""
"The descriptions view can be used to type or paste in a title and/or a "
"captions of unlimited size (see note below). The text is UTF-8 compatible, "
"meaning that all special characters are allowed. The captions are copied to "
"Exif, IPTC, and XMP fields to be used by other applications."
msgstr ""
"La vista Descrizione può essere utilizzata per scrivere o incollare un "
"titolo e/o una didascalia di dimensioni quasi illimitate (vedi nota "
"riportata sotto). Il testo è compatibile con UTF-8, dunque sono permessi "
"tutti i caratteri speciali. Le didascalie vengono copiate nei campi Exif, "
"IPTC e XMP in modo da poter essere utilizzate dalle altre applicazioni."

#: ../../right_sidebar/captions_view.rst:38
msgid ""
"IPTC data only supports ASCII characters and is limited to 2000 characters "
"(old American norm). All texts will be truncated after 2000 chars, and "
"special characters will be malformed. If you intend to use the IPTC caption "
"field in other applications you should be compliant with these restrictions."
msgstr ""
"I dati IPTC supportano soltanto i caratteri ASCII, che sono limitati a 2000 "
"(vecchia norma americana). Tutti i testi saranno tagliati dopo 2000 "
"caratteri, e i caratteri speciali saranno storpiati. Se vuoi usare il campo "
"delle didascalie IPTC in altre applicazioni dovresti rispettare queste "
"limitazioni."

#: ../../right_sidebar/captions_view.rst:40
msgid "Title and caption editors are powerful tool which supports:"
msgstr ""
"Gli editor del titolo e della didascalia sono strumenti avanzati che "
"supportano:"

#: ../../right_sidebar/captions_view.rst:42
msgid "Multiple alternative language strings."
msgstr "Più stringhe di lingua alternative."

#: ../../right_sidebar/captions_view.rst:43
msgid "Translate strings online to another language."
msgstr "Traduzione di stringhe in linea in un'altra lingua."

#: ../../right_sidebar/captions_view.rst:44
msgid "Author strings definition."
msgstr "Definizione delle stringhe per l'autore."

#: ../../right_sidebar/captions_view.rst:46
msgid ""
"The default language from an alternative strings stack is **x-default** and "
"must be written by preference in English. If one string must be entered in "
"the stack, the x-default must be present in prior."
msgstr ""
"La lingua predefinita da una pila di stringhe alternative è **x-default** e "
"deve essere scritta preferibilmente in inglese. Se nella pila deve essere "
"inserita una stringa, x-default deve essere già presente."

#: ../../right_sidebar/captions_view.rst:48
msgid ""
"After commenting, either choose the **Apply** button or go straight to the "
"next image, the descriptions will be saved."
msgstr ""
"Dopo aver commentato, scegli o il pulsante **Applica** o vai direttamente "
"all'immagine successiva, e le descrizioni saranno salvate."

#: ../../right_sidebar/captions_view.rst:50
msgid ""
"Next to the Apply button there is the **More** button. From it you can "
"either choose to read metadata from the selected file to the database, or "
"the other way around, to write metadata to the files (the latter take place "
"anyway if you chose a metadata setting so that all metadata is always saved "
"to the images)."
msgstr ""
"Accanto al pulsante Applica è presente un pulsante **Altro**. Da esso puoi "
"leggere i metadati dal file selezionato e trasferirli alla banca dati, "
"oppure, all'inverso, di scrivere i metadati nei file (questa opzione viene "
"eseguita comunque, se hai scelto un'impostazione per salvare sempre i "
"metadati nelle immagini)."

#: ../../right_sidebar/captions_view.rst:55
msgid "Date and Time"
msgstr "Data e ora"

#: ../../right_sidebar/captions_view.rst:57
msgid ""
"In the Date and Time section, which reflects the time of taking the "
"photograph, you can change all values. From the date combo-box a calendar "
"opens, and the time setting spin-box can also be written by directly typing "
"the time. The dating is copied to the Exif **Date and Time** field. If you "
"need to change a number of images for their creating time & date, there is a "
"more comfortable method available in **Batch Queue Manager** or from :"
"menuselection:`Item --> Adjust time & date...` menu entry in **Main "
"Window**. Select the images to be changed in the main view and call the tool."
msgstr ""
"Nella sezione Data e ora, che rispecchia la data di scatto della fotografia, "
"puoi modificare tutti i valori. Dalla casella combinata della data si apre "
"un calendario, e la casella di scelta dell'ora può essere anche sovrascritta "
"direttamente a mano. La datazione è copiata nel campo **Date and Time** "
"Exif. Se hai necessità di cambiare la data e l'ora di creazione di una serie "
"di immagini, è disponibile un sistema più comodo per farlo nel **Gestore "
"elaborazione in serie** o dalla voce di menu :menuselection:`Elemento --> "
"Regola ora e data...` nella **finestra principale**. Seleziona le immagini "
"da modificare nella vista principale e richiama lo strumento."

#: ../../right_sidebar/captions_view.rst:62
msgid "Labels"
msgstr "Etichette"

#: ../../right_sidebar/captions_view.rst:64
msgid ""
"The Rating section displays a 0...5 star rating scheme that can be used in "
"searches and sort orders. It can be applied by a single mouse click to the 5 "
"stars in the sidebar or with a keyboard shortcut :kbd:`Ctrl+0...5`. The "
"rating from the sidebar is always applied to one image at a time. To rate a "
"number of images, select them and pop-up the context menu (click with the "
"right mouse button) to apply a common rating."
msgstr ""
"La sezione Valutazione visualizza uno schema di voto da zero a cinque "
"stelle, che può essere usato nelle ricerche e nei riordinamenti. Si applica "
"con un singolo clic del mouse sulle cinque stelle nella barra laterale, o "
"con la scorciatoia da tastiera :kbd:`Ctrl+0...5`. Il voto della barra "
"laterale viene sempre applicato a un'immagine alla volta. Per dare un voto a "
"un certo numero d'immagini, selezionale e fai apparire il menu contestuale "
"(clic con il pulsante destro del mouse) per applicare una valutazione comune."

#: ../../right_sidebar/captions_view.rst:66
msgid ""
"The labels view allow to assign also the Color and the Pick tags that you "
"can use in your workflow to classify items."
msgstr ""
"La vista Etichette consente di assegnare i tag di colore e di scelta da "
"utilizzare nel tuo lavoro per classificare gli elementi."

#: ../../right_sidebar/captions_view.rst:68
msgid ""
"The rating is then transcribed into the IPTC *urgency* data field. The "
"transcoding follows the scheme in this table:"
msgstr ""
"Il voto viene quindi trascritto nel campo di dati IPTC **Urgency**. La "
"codifica segue lo schema della tabella seguente:"

#: ../../right_sidebar/captions_view.rst:71
msgid "digiKam Rating"
msgstr "Valutazione digiKam"

#: ../../right_sidebar/captions_view.rst:71
msgid "IPTC Urgency"
msgstr "Urgency di IPTC"

#: ../../right_sidebar/captions_view.rst:73
msgid "no star"
msgstr "nessuna stella"

#: ../../right_sidebar/captions_view.rst:73
msgid "8"
msgstr "8"

#: ../../right_sidebar/captions_view.rst:74
#: ../../right_sidebar/captions_view.rst:75
msgid "1 star"
msgstr "1 stella"

#: ../../right_sidebar/captions_view.rst:74
msgid "7"
msgstr "7"

#: ../../right_sidebar/captions_view.rst:75
msgid "6"
msgstr "6"

#: ../../right_sidebar/captions_view.rst:76
msgid "2 stars"
msgstr "2 stelle"

#: ../../right_sidebar/captions_view.rst:76
msgid "5"
msgstr "5"

#: ../../right_sidebar/captions_view.rst:77
msgid "3 stars"
msgstr "3 stelle"

#: ../../right_sidebar/captions_view.rst:77
msgid "4"
msgstr "4"

#: ../../right_sidebar/captions_view.rst:78
#: ../../right_sidebar/captions_view.rst:79
msgid "4 stars"
msgstr "4 stelle"

#: ../../right_sidebar/captions_view.rst:78
msgid "3"
msgstr "3"

#: ../../right_sidebar/captions_view.rst:79
msgid "2"
msgstr "2"

#: ../../right_sidebar/captions_view.rst:80
msgid "5 stars"
msgstr "5 stelle"

#: ../../right_sidebar/captions_view.rst:80
msgid "1"
msgstr "1"

#: ../../right_sidebar/captions_view.rst:86
msgid "Tags Tree"
msgstr "Albero dei tag"

#: ../../right_sidebar/captions_view.rst:88
msgid ""
"The tag view shows an adaptive filter tag search box, the tag tree and a "
"combo-box containing the tags previously applied in this digiKam session."
msgstr ""
"La vista Tag mostra un riquadro di ricerca con filtro adattativo dei tag, "
"l'albero dei tag e una casella combinata contenente i tag applicati in "
"precedenza nella presente sessione di digiKam."

#: ../../right_sidebar/captions_view.rst:94
msgid "The Metadata View From Right Sidebar Displaying Tags Information"
msgstr ""
"La vista dei metadati dalla barra laterale destra che visualizza le "
"informazioni dei tag"

#: ../../right_sidebar/captions_view.rst:96
msgid ""
"The tag tree will be adapted dynamically as a function of the search word as "
"you type into the box. So it is easy to quickly reduce the number of "
"possibilities when searching for a tag. Of course, this feature is only "
"useful if you have many tags."
msgstr ""
"L'albero dei tag si adatterà dinamicamente in funzione della parola di "
"ricerca mentre la scrivi nel riquadro. È quindi facile ridurre rapidamente "
"il numero di possibilità quando cerchi un tag. Ovviamente, questa "
"funzionalità è utile solo se hai molti tag."

#: ../../right_sidebar/captions_view.rst:98
msgid ""
"The combo-box at the bottom is another ergonomic feature for easy tagging of "
"an image series. As you apply different tags they will be memorized in this "
"box for quick access."
msgstr ""
"La casella combinata in fondo è un'altra funzionalità ergonomica per "
"classificare facilmente una serie di immagini. Quando applichi diversi tag, "
"essi saranno memorizzati in questo riquadro per potervi accedere rapidamente "
"in seguito."

#: ../../right_sidebar/captions_view.rst:100
msgid ""
"Otherwise tags are simply applied by checking the respective boxes in the "
"tree. All tags of an image are transcribed into the XMP/IPTC *keywords* data "
"field."
msgstr ""
"Altrimenti, i tag sono applicati semplicemente segnandone i rispettivi "
"riquadri nell'albero. Tutti i tag di un'immagine sono trascritti nel campo "
"di dati delle parole chiave XMP/IPTC."

#: ../../right_sidebar/captions_view.rst:104
msgid ""
"In case you have selected a number of images in the Icon-View and you check "
"a tag in the tag tree, this one is only applied to the highlighted images, "
"and not to the whole Album contents."
msgstr ""
"Nel caso in cui tu abbia selezionato diverse immagini nella vista a icone, e "
"marcato un tag nell'albero dei tag, quest'ultimo viene applicato solo alle "
"immagini evidenziate e non all'intero contenuto dell'album."

#: ../../right_sidebar/captions_view.rst:109
msgid "Information View"
msgstr "Vista Informazioni"

#: ../../right_sidebar/captions_view.rst:115
msgid "The Metadata View From Right Sidebar Displaying Template Information"
msgstr ""
"La vista dei metadati dalla barra laterale destra che visualizza le "
"informazioni sul modello"

#: ../../right_sidebar/captions_view.rst:117
msgid ""
"The Information view allows to assign a template of textuals information to "
"items in one pass. This information can be populated in Setup/Template "
"dialog. They contain all strings describing the contents, the scene, the "
"authors, the rights, the place etc."
msgstr ""
"La vista Informazioni consente di assegnare un modello di informazioni di "
"testo agli elementi in un colpo solo. Queste informazioni possono essere "
"popolate nella finestra di dialogo Configurazione/Modello. Esse contengono "
"tutte le stringhe descrittive del contenuto, la scena, gli autori, i "
"diritti, il luogo, ecc."

#: ../../right_sidebar/captions_view.rst:119
msgid ""
"For more details about Template, see the description of the :ref:`Template "
"Settings <templates_settings>` section."
msgstr ""
"Per ulteriori dettagli sui modelli, vedi la descrizione nella sezione :ref:"
"`Impostazioni dei modelli <templates_settings>`."
