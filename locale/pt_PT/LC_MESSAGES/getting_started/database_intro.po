# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-09 00:53+0000\n"
"PO-Revision-Date: 2023-02-17 15:53+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: digiKam PGF ref\n"

#: ../../getting_started/database_intro.rst:1
msgid "How to quickly start digiKam photo management program"
msgstr "Como iniciar rapidamente o programa de gestão de fotografias digiKam"

#: ../../getting_started/database_intro.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, database, intro"
msgstr ""
"digiKam, documentação, manual do utilizador, gestão de fotografias, código "
"aberto, livre, ajuda, aprender, fácil, base de dados, introdução"

#: ../../getting_started/database_intro.rst:14
msgid "Database"
msgstr "Base de Dados"

#: ../../getting_started/database_intro.rst:16
msgid ""
"Everyone knows about a database; it is used to store data. As all other "
"photographs management programs, digiKam too uses the database for some "
"obvious reasons like avoiding data duplication, reducing data redundancy, a "
"quick search engine, and greater data integrity. Moreover, the cost of data "
"entry, storage and retrieval are drastically reduced. Additionally, any user "
"can access the data using query language."
msgstr ""
"Todos sabem o que é uma base de dados; serve para guardar dados, como ela "
"própria indica. Como em todos os programas de gestão de fotografias, o "
"digiKam também usa a base de dados para algumas razões óbvias, como evitar a "
"duplicação de dados, reduzindo a redundância dos mesmos, um motor de busca "
"rápido e uma maior integridade dos dados. Para além disso, o custo da "
"introdução, armazenamento e consulta dos dados são drasticamente reduzidos. "
"Para além disso, qualquer utilizador consegue aceder aos dados através de "
"uma linguagem de pesquisas."

#: ../../getting_started/database_intro.rst:18
msgid ""
"Talking in particular about digiKam, the Albums, Album Roots, Tags, "
"Thumbnails, Face Recognition Data, Image Metadata, File Paths, Settings etc. "
"are all stored in different database files."
msgstr ""
"Falando em particular do digiKam, os Álbuns, Raízes dos Álbuns, Marcas, "
"Miniaturas, Dados de Reconhecimento de Caras, Meta-Dados das Imagens, "
"Localizações dos Ficheiros, Configuração, etc., são todos guardados em "
"ficheiros diferentes da base de dados."

#: ../../getting_started/database_intro.rst:20
msgid ""
"The digiKam actually manages more than one database. For convenience, it is "
"broadly categorized in three:"
msgstr ""
"O digiKam consegue este momento gerir mais que uma base de dados. Por "
"conveniência, está amplamente categorizado em três:"

#: ../../getting_started/database_intro.rst:22
msgid ""
"Core database for all collection properties, i.e. it hosts all albums, "
"images and searches data."
msgstr ""
"A base de dados de base para todas as propriedades da colecção, i.e., aloja "
"todos os álbuns, imagens e pesquisa nos dados."

#: ../../getting_started/database_intro.rst:24
msgid ""
"Thumbnails database for compressed thumbnails i.e. to host image thumbs "
"using wavelets compression images (**PGF** format)."
msgstr ""
"A base de dados das miniaturas comprimidas, i.e., para alojar miniaturas de "
"imagens que usam imagens comprimidas com padrões de ondas (formato **PGF**)."

#: ../../getting_started/database_intro.rst:26
msgid ""
"Similarity database to store image finger-prints for fuzzy search engine."
msgstr ""
"A base de dados de semelhanças para guardar as impressões digitais das "
"imagens para o motor de pesquisas difusas."

#: ../../getting_started/database_intro.rst:28
msgid ""
"Face database for storing face recognition metadata i.e. to host face "
"histograms for faces recognition."
msgstr ""
"Base de dados de caras para guardar os meta-dados de reconhecimento de "
"caras, i.e. para guardar os histogramas de reconhecimento de caras."

#: ../../getting_started/database_intro.rst:34
msgid ""
"Example of digiKam Remote MySQL Configuration Hosted on a NAS From The Local "
"Network"
msgstr ""
"Exemplo da Configuração do MySQL Remoto do digiKam, Alojado num NAS da Rede "
"Local"

#: ../../getting_started/database_intro.rst:36
msgid ""
"The whole details of database settings are mostly given in the :ref:"
"`database setup section <database_settings>`."
msgstr ""
"Os detalhes completos da configuração da base de dados são apresentados na "
"sua maioria na :ref:`secção de configuração da base de dados "
"<database_settings>`."
