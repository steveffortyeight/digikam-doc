# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-05 00:50+0000\n"
"PO-Revision-Date: 2023-02-07 18:33+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: digiKam ref\n"

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid "digiKam Maintenance Tool to Find Duplicates"
msgstr "Ferramenta de Manutenção do digiKam para Pesquisa de Duplicados"

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, maintenance, duplicates, similarity"
msgstr ""
"digiKam, documentação, manual do utilizador, gestão de fotografias, código "
"aberto, livre, ajuda, aprender, fácil, manutenção, duplicados, semelhança"

#: ../../maintenance_tools/maintenance_duplicates.rst:14
msgid "Find Duplicates"
msgstr "Procurar Duplicados"

#: ../../maintenance_tools/maintenance_duplicates.rst:16
msgid "Contents"
msgstr "Conteúdo"

#: ../../maintenance_tools/maintenance_duplicates.rst:22
msgid "The digiKam Maintenance Options to Find Duplicates"
msgstr "A Ferramenta de Manutenção do digiKam para Pesquisa de Duplicados"

#: ../../maintenance_tools/maintenance_duplicates.rst:24
msgid ""
"The **Find Duplicates** Tool is doing the same as the Find duplicates button "
"in the :ref:`the Similarity View <similarity_view>`, but here you can "
"combine it with other maintenance operations and you have the chance to "
"check **Work on all processor cores** under :ref:`Common Options "
"<maintenance_common>` to speed up the process."
msgstr ""
"A ferramenta de **Pesquisa de Duplicados** faz o mesmo que o botão para "
"Procurar Duplicados na :ref:`área de Semelhanças <similarity_view>`, mas "
"aqui poderá combiná-la com outras ferramentas de manutenção e terá a "
"hipótese de **Trabalhar em todos os núcleos de processamento** nas :ref:"
"`Opções Comuns <maintenance_common>` para acelerar o processo."

#: ../../maintenance_tools/maintenance_duplicates.rst:30
msgid "The digiKam Find Duplicates Button from Similarity Left Sidebar"
msgstr ""
"O Botão para Detectar Duplicados do digiKam na Barra Lateral Esquerda das "
"Semelhanças"

#: ../../maintenance_tools/maintenance_duplicates.rst:32
msgid "This process provides two options to find duplicates items:"
msgstr "Este processo fornece duas opções para procurar itens duplicados:"

#: ../../maintenance_tools/maintenance_duplicates.rst:34
msgid ""
"**Similarity Range**: the lower and higher values to define the range of "
"similarity in percents."
msgstr ""
"**Gama de Semelhanças**: os valores inferior e superior para definir a gama "
"de semelhanças em termos percentuais."

#: ../../maintenance_tools/maintenance_duplicates.rst:36
msgid ""
"**Restriction**: this option restrict the duplicate search with some "
"criteria, as to limit search to the album of reference image, or to exclude "
"the album of reference image of the search."
msgstr ""
"**Restrição**: esta opção restringe a pesquisa de duplicados com alguns "
"critérios, de forma a limitar a pesquisa ao álbum da imagem de referência ou "
"para excluir o mesmo da pesquisa."

#: ../../maintenance_tools/maintenance_duplicates.rst:38
msgid ""
"While the find duplicates process is under progress, notification on the "
"bottom right of main windows will be visible to indicate the amount of items "
"already done."
msgstr ""
"Enquanto o processo de pesquisa de duplicados estiver em curso, estará "
"visível uma notificação no canto inferior direito das janelas principais "
"para indicar o número de itens já processados."

#: ../../maintenance_tools/maintenance_duplicates.rst:44
msgid "The digiKam Find Duplicates Process Running in the Background"
msgstr ""
"O Processo de Pesquisa de Duplicados do digiKam a Executar em Segundo Plano"
