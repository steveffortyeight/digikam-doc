# Translation of docs_digikam_org_maintenance_tools___maintenance_faces.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-05 00:50+0000\n"
"PO-Revision-Date: 2023-02-05 18:58+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.1.1\n"

#: ../../maintenance_tools/maintenance_faces.rst:1
msgid "digiKam Maintenance Tool Detect and Recognize Faces"
msgstr "L'eina de manteniment Detecta i reconèix les cares en el digiKam"

#: ../../maintenance_tools/maintenance_faces.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, maintenance, faces, detection, recognition, deep-learning"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, manteniment, cares, detecció, "
"reconeixement, aprenentatge profund"

#: ../../maintenance_tools/maintenance_faces.rst:14
msgid "Detect and Recognize Faces"
msgstr "Detectar i reconèixer les cares"

#: ../../maintenance_tools/maintenance_faces.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../maintenance_tools/maintenance_faces.rst:22
msgid "The digiKam Maintenance Options to Detect and Recognize Faces"
msgstr ""
"Les opcions de manteniment per a «Detecta i reconeix les cares» en el digiKam"

#: ../../maintenance_tools/maintenance_faces.rst:24
msgid ""
"This is the same process you can access in the **People** view with the "
"**Scan Collection for Faces** button. Here you just cannot set the **Fast - "
"Accurate** parameter but just the **Face Data Management** method and the "
"option to **Clear and rebuild all training data**. For more information "
"about the **Faces Data Management** options see :ref:`the People View "
"section <people_view>`."
msgstr ""
"Aquest és el mateix procés al qual podreu accedir a la vista **Persones** "
"amb el botó **Explora la col·lecció cercant cares**. Aquí senzillament no "
"podreu establir el paràmetre **Ràpida - Acurada**, sinó només el mètode per "
"a la **Gestió de les dades de cares** i l'opció **Neteja i reconstrueix "
"totes les dades d'entrenament**. Per a obtenir més informació sobre les "
"opcions de **Gestió de les dades de cares**, vegeu la :ref:`secció Vista de "
"persones <people_view>`."

#: ../../maintenance_tools/maintenance_faces.rst:30
msgid "The Scan Collection for Faces Button from People Left Sidebar"
msgstr ""
"El botó «Explora la col·lecció cercant cares» des de la barra lateral "
"esquerra Persones"

#: ../../maintenance_tools/maintenance_faces.rst:32
msgid ""
"While the faces management process is under progress, notification on the "
"bottom right of main windows will be visible to indicate the amount of items "
"already done."
msgstr ""
"Mentre el procés de gestió de les cares estigui en progrés, es veurà una "
"notificació a la part inferior dreta de la finestra principal per a indicar "
"la quantitat d'elements que ja s'han fet."

#: ../../maintenance_tools/maintenance_faces.rst:38
msgid "The digiKam Faces Management Process Running in the Background"
msgstr "El procés de gestió de les cares en el digiKam treballa en segon pla"

#: ../../maintenance_tools/maintenance_faces.rst:42
msgid ""
"To run properly, the process needs the deep-learning models to download at "
"the first run of digiKam. See :ref:`the Quick Start section <quick_start>` "
"for details."
msgstr ""
"Per a funcionar correctament, el procés necessita que es baixin els models "
"d'aprenentatge profund durant la primera execució del digiKam. Per a obtenir "
"més informació, vegeu la :ref:`secció Inici ràpid <quick_start>`."
